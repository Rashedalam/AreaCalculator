<?php

namespace Pondit\Calculator\AreaCalculator;

class Square
{
    public $arm;

    public function get_area()
    {
        return $this->arm * $this->arm;
    }
}
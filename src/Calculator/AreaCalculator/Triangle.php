<?php

namespace Pondit\Calculator\AreaCalculator;

class Triangle
{
    public $base;
    public $height;

    public function get_area()
    {
        return 1 / 2 * $this->base * $this->height;
    }
}
<?php

namespace Pondit\Calculator\AreaCalculator;
class Rectangle
{
    public $width;
    public $length;

    public function get_area()
    {
        return $this->width * $this->length;
    }
}
<?php

namespace Pondit\Calculator\AreaCalculator;
class Circle
{
    public $radius;
    public $pi;

    public function get_area()
    {
        return $this->pi * $this->radius * $this->radius;
    }
}
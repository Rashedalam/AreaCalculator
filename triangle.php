<?php

include_once 'vendor/autoload.php';

use Pondit\Calculator\AreaCalculator\Triangle;
use Pondit\Calculator\AreaCalculator\Displayer;

$triangle=new Triangle();
$triangle->height=10;
$triangle->base=12;

$displayer=new Displayer();
$displayer->displayerH1("The area of triangle is : ".$triangle->get_area());
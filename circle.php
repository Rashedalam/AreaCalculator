<?php
include_once 'vendor/autoload.php';

use Pondit\Calculator\AreaCalculator\Circle;
use Pondit\Calculator\AreaCalculator\Displayer;

$circle=new Circle();
$circle->radius=5;
$circle->pi=3.1416;

$displayer=new Displayer();
$displayer->displayerH1("The are of circle is : ".$circle->get_area());
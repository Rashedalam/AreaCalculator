<?php
    include_once 'vendor/autoload.php';

    use Pondit\Calculator\AreaCalculator\Rectangle;
    use Pondit\Calculator\AreaCalculator\Displayer;

    $rectangle=new Rectangle();
    $rectangle->length=10;
    $rectangle->width=20;
    $result=$rectangle->get_area();

    $displayer=new Displayer();
    $displayer->displayerH1("The area of rectangle is : ".$result);
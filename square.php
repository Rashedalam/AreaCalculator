<?php
include_once 'vendor/autoload.php';

use Pondit\Calculator\AreaCalculator\Square;
use Pondit\Calculator\AreaCalculator\Displayer;

$square=new Square();
$square->arm=10;
$displayer=new Displayer();
$displayer->displayerH1("The area of square is : ".$square->get_area());
